/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c;

import de.elomagic.core.app.ConsoleAppParameter;

/**
 * Supported console parameters.
 */
public enum SupportedParameters implements ConsoleAppParameter {

    // Commands
    CmdCreateTemplateSample("cts", "createTemplateSample", "Creates a template sample in the working folder.", true),
    CmdEncryptPassword("ep", "encryptPassword", "Encrypt the given password.", true, "password"),
    CmdExport("ex", "export", "Export template.", true),
    CmdHelp("h", "help", "Shows all available command line arguments.", true),
    CmdImport("in", "import", "Imports project settings as Jira project properties.", true),
    CmdPreviewPage("p", "preview", "Preview Confluence page. Useful to test the \"jsoup\" query.", true),
    CmdPrintPage("pp", "printPage", "Prints only the Confluence page by using the Jira project key or Confluence page ID.", true),
    CmdPrintProjectProperties("ppp", "printProjectProperties", "(Command) Prints only the Jira project properties by the Jira project Key.", true),
    CmdRelease("r", "release", "Will record the project key as released in Confluence and Jira.", true),
    CmdValidateTemplate("vt", "validate", "Validate template", true),
    CmdVersion("V", "appVersion", "Prints version of this tool", true),
    // Options
    OptConfigurationFile("cf", "config", "Configuration file.", false, "file"),
    OptConfluencePassword("cp", "confluencePassword", "Password of the Confluence account.", false, "password"),
    OptConfluencePageId("id", "confluencePageId", "Confluence page id.", false, "id"),
    OptConfluenceUrl("curl", "confluenceUrl", "URL of the Confluence web-server.", false, "url"),
    OptConfluenceUsername("cu", "confluenceUsername", "Username of the Confluence account.", false, "username"),
    OptCustomVariable("C", "customVariable", "Custom variable used in the template.", false, "key", "value"),
    OptJiraPassword("jp", "jiraPassword", "Password of the Jira account.", false, "password"),
    OptJiraProjectKey("key", "jiraProjectKey", "Jira project key.", false, "key"),
    OptJiraUrl("jurl", "jiraUrl", "URL of the Jira web-server.", false, "url"),
    OptJiraUsername("ju", "jiraUsername", "Username of the Jira account.", false, "username"),
    OptJSoupQuery("jsq", "jsoupQuery", "Jsoup query to select table in Confluence page.", false, "query"),
    OptReleaseComment("rc", "releaseComment", "Comment fo the release. Default: empty", false, "comment"),
    OptReleaseDate("rd", "releaseDate", "Date of the release. Sample: 2017-12-01", false, "date"),
    OptSkipConfluence("sc", "skipConfluence", "When in use then version isn't record in Confluence", false),
    OptSkipJira("sj", "skipJira", "When in use then version isn't released in Jira", false),
    OptTemplateFile("tf", "templateFile", "File of the template file.", false, "file"),
    OptVerbose("v", "verbose", "Use this option to get more log output.", false),
    OptVersion("ver", "version", "Version of the project. Sample: 6.3.0.1", false, "version");

    private final String shortName;
    private final String longName;
    private final String description;
    private final boolean command;
    private final String[] args;

    private SupportedParameters(String shortName, String longName, String description, boolean command, String... args) {
        this.shortName = shortName;
        this.longName = longName;
        this.description = description;
        this.command = command;
        this.args = args;
    }

    @Override
    public String getShortName() {
        return shortName;
    }

    @Override
    public String getLongName() {
        return longName;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String[] getArguments() {
        return args;
    }

    /**
     * Returns type of argument.
     *
     * @return Returns true when argument if a command
     */
    @Override
    public boolean isCommand() {
        return command;
    }

}

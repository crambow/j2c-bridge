/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c.services;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.apache.http.client.HttpResponseException;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.engines.ApacheHttpClient4Engine;

import de.elomagic.j2c.settings.dto.CredentialsSetting;

/**
 *
 * @author Carsten Rambow
 */
public abstract class AbstractService {

    protected enum FailMode {
        ReturnNull,
        ReturnEmptyInstance,
        ThrowException
    }

    private Client client;

    protected abstract CredentialsSetting getCredentials();

    protected abstract String getServiceURL() throws Exception;

    public Client getClient() throws Exception {
        if(client == null) {
            CredentialsSetting credentials = getCredentials();

            ApacheHttpClient4Engine engine = new ApacheHttpClient4Engine(HttpClientFactory.build());
            client = new ResteasyClientBuilder().httpEngine(engine).build();
            client.register(new Authenticator(credentials.getUsername(), credentials.getPasswordDecrypted()));
        }

        return client;
    }

    protected void validateAuthenticationResponse(Response response) throws Exception {

        int httpStatus = response.getStatus();
        if(httpStatus == HttpStatus.SC_UNAUTHORIZED) {
            throw new HttpResponseException(HttpStatus.SC_UNAUTHORIZED, "Authorization for \"" + getServiceURL() + "\" failed. HTTP status: 401");
        }

    }

    /**
     *
     * @param <T>
     * @param response
     * @param clazz
     * @param mode In case when no status 2xx was response then method behave like the mode defined
     * @return
     * @throws Exception
     */
    protected <T> T proceedResponse(Response response, Class<T> clazz, FailMode mode) throws Exception {

        validateAuthenticationResponse(response);

        T result;

        int httpStatus = response.getStatus();
        if(httpStatus >= HttpStatus.SC_OK && httpStatus < HttpStatus.SC_MULTI_STATUS) {
            result = clazz == Void.class ? null : response.readEntity(clazz);
        } else if(mode == FailMode.ReturnEmptyInstance) {
            result = clazz.newInstance();
        } else if(mode == FailMode.ReturnNull) {
            result = null;
        } else {
            throw new HttpResponseException(httpStatus, "HTTP request failed on \"" + getServiceURL() + "\". HTTP status: " + httpStatus + " " + response.getStatusInfo().getReasonPhrase());
        }

        response.close();

        return result;
    }

}

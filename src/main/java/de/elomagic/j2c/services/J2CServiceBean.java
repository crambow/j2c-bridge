/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c.services;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.util.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import de.elomagic.j2c.ApplicationProperties;
import de.elomagic.j2c.services.confluence.ConfluenceService;
import de.elomagic.j2c.services.confluence.entities.ContentResponse;
import de.elomagic.j2c.services.jira.JiraService;
import de.elomagic.j2c.services.jira.entities.Project;
import de.elomagic.j2c.services.jira.entities.ProjectPropertiesKey;
import de.elomagic.j2c.services.jira.entities.ProjectPropertiesResponse;
import de.elomagic.j2c.services.jira.entities.ProjectProperty;
import de.elomagic.j2c.services.jira.entities.Version;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateMethodModelEx;

public class J2CServiceBean {

    private static final Logger LOGGER = LogManager.getLogger(J2CServiceBean.class);

    private static final String J2C_CONFLUENCE_COMMENT = "j2c.comment";
    private static final String J2C_CONFLUENCE_PAGE_ID = "j2c.confluencePageId";
    private static final String J2C_CONFLUENCE_RECORD_TEMPLATE = "j2c.confluenceRecordTemplate";
    private static final String J2C_CONFLUENCE_JSOUP_QUERY = "j2c.confluenceJsoupQuery";
    private static final String J2C_VERSION = "j2c.version";
    
    @Inject
    private ApplicationProperties applicationProperties;
    @Inject
    private JiraService jiraService;
    @Inject
    private ConfluenceService confluenceService;

    public J2CServiceBean() throws Exception {
    }

    public void recordReleasedVersion(
            String projectKey,
            String confluencePageId,
            String confluenceJsoupQuery,
            String template,
            String version,
            boolean skipJira,
            boolean skipConfluence,
            LocalDate releaseDate,
            String releaseComment,
            Map<String, String> customVariables) throws Exception {
        if(!StringUtils.hasText(version)) {
            throw new Exception("Version isn't set.");
        }

        if(skipConfluence && skipJira) {
            LOGGER.warn("Both options \"skipJira\" and \"skipConfluence\" are set. Nothing will happens!");
        }

        if(!skipJira) {
            recordInJira(projectKey, version, releaseDate, releaseComment);
        }

        if(!skipConfluence) {
            recordInConfluence(projectKey, confluencePageId, confluenceJsoupQuery, template, version, releaseComment, customVariables);
        }
    }

    public String readPageContent(String confluencePageId) throws Exception {
        ContentResponse contentResponse = confluenceService.getContentBodyStorage(confluencePageId);

        String body = contentResponse.getBody().getStorage().getValue();

        return body;
    }

    public Set<ProjectProperty> readProjectPropertiesFromJira(String jiraProjectKey) throws Exception {

        ProjectPropertiesResponse response = jiraService.getProjectProperties(jiraProjectKey);

        Set<ProjectPropertiesKey> set = response.getKeys() == null ? Collections.emptySet() : new HashSet<>(Arrays.asList(response.getKeys()));

        Set<ProjectProperty> result = set
                .stream()
                .map(key->getProjectProperty(jiraProjectKey, key.getKey()))
                .collect(Collectors.toSet());

        return result;
    }

    private ProjectProperty getProjectProperty(String projectKey, String property) throws RuntimeException {
        try {
            return jiraService.getProjectProperty(projectKey, property, null);
        } catch(Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private void importProjectProperty(String jiraProjectKey, String property, String value) throws Exception {
        LOGGER.debug("Writing property \"" + property + "\" of project \"" + jiraProjectKey + "\" into Jira...");
        jiraService.setProjectProperty(jiraProjectKey, J2C_CONFLUENCE_COMMENT, applicationProperties.getName() + " by Carsten Rambow");
        jiraService.setProjectProperty(jiraProjectKey, J2C_VERSION, applicationProperties.getVersion());
        jiraService.setProjectProperty(jiraProjectKey, property, value);
    }

    public void importPageId(String jiraProjectKey, String confluencePageId) throws Exception {
        importProjectProperty(jiraProjectKey, J2C_CONFLUENCE_PAGE_ID, confluencePageId);
    }

    public void importRecordTemplate(String jiraProjectKey, Path rowTemplateFile, Map<String, String> customVariables) throws Exception {
        LOGGER.debug("Importing template file \"" + rowTemplateFile + "\" into \"" + jiraProjectKey + "\"...");
        String template = readTemplate(rowTemplateFile);

        fillTemplate(jiraProjectKey, template, "1.2.3.4", "release-comment", customVariables);

        importProjectProperty(jiraProjectKey, J2C_CONFLUENCE_RECORD_TEMPLATE, template);
    }

    public void importJSoupQuery(String jiraProjectKey, String jsoupQuery) throws Exception {
        importProjectProperty(jiraProjectKey, J2C_CONFLUENCE_JSOUP_QUERY, jsoupQuery);
    }

    private String readProjectPropertyFromJira(String jiraProjectKey, String property, String defaultValue) throws Exception {
        LOGGER.debug("Reading property \"" + property + "\" of project \"" + jiraProjectKey + "\" from Jira...");
        String value = jiraService.getProjectProperty(jiraProjectKey, property, defaultValue).getValue();
        return value;
    }

    public String readConfluencePageIdFromJira(String jiraProjectKey) throws Exception {
        return readProjectPropertyFromJira(jiraProjectKey, J2C_CONFLUENCE_PAGE_ID, "");
    }

    /**
     * Reads the template from the Jira project properties.
     *
     * @param jiraProjectKey
     * @return
     * @throws Exception
     */
    public String readTemplateFromJira(String jiraProjectKey) throws Exception {
        return readProjectPropertyFromJira(jiraProjectKey, J2C_CONFLUENCE_RECORD_TEMPLATE, "");
    }

    public String readJsoupQueryFromJira(String jiraProjectKey) throws Exception {
        return readProjectPropertyFromJira(jiraProjectKey, J2C_CONFLUENCE_JSOUP_QUERY, "");
    }

    /**
     * Reads the template from a local file.
     *
     * @param templateFile File to read
     * @return Returns the content of the file.
     * @throws IOException Thrown when unable to read the file.
     */
    public String readTemplate(Path templateFile) throws IOException {
        LOGGER.info("Reading template file \"" + templateFile + "\"...");
        try (Reader reader = Files.newBufferedReader(templateFile, StandardCharsets.UTF_8)) {
            return IOUtils.toString(reader);
        }
    }

    public void writeTemplate(Path templateFile, String templateHTML) throws IOException {
        if(templateFile != null) {
            LOGGER.debug("Writing template of into \"" + templateFile + "\"...");
            try (Writer writer = Files.newBufferedWriter(templateFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)) {
                writer.write(templateHTML);
            }
        }
    }

    public void createSample(Path templateFile) throws Exception {
        // TODO How to export next line ?!?
        // projectSetting.setConfluenceJsoupQuery("table \\u003e tbody \\u003e tr");

        String html = IOUtils.toString(getClass().getResource("/de/elomagic/j2c/samples/template-sample-html.ftlh"), StandardCharsets.UTF_8);

        writeTemplate(templateFile, html);
    }

    public void exportTemplateFromJira(String projectKey, Path templateFile) throws Exception {
        String template = readTemplateFromJira(projectKey);

        writeTemplate(templateFile, template);
    }

    private void recordInJira(String jiraProjectKey, String versionName, LocalDate releaseDate, String releaseComment) throws RuntimeException {

        try {
            Version[] versions = jiraService.getVersionsOfProject(jiraProjectKey);

            Version version = Arrays.stream(versions)
                    .filter(v->versionName.equals(v.getName()))
                    .findFirst()
                    .orElse(null);

            if(version == null) {
                // Getting project ID
                Project project = jiraService.getProjectByKey(jiraProjectKey);

                version = new Version();
                version.setProjectId(project.getId());
                version.setProject(jiraProjectKey);
                version.setName(versionName);
                version.setReleased(true);
                version.setReleaseDate(releaseDate);
                version.setDescription(releaseComment);
                version.setArchived(false);

                jiraService.createVersion(version);
            } else if(version.isReleased()) {
                // Dont' overwrite already released version ?!?
                LOGGER.debug("Skipping update of version. " + jiraProjectKey + " version " + versionName + " already released.");
            } else {
                version.setProject(jiraProjectKey);
                version.setReleased(true);
                version.setReleaseDate(releaseDate);
                version.setDescription(releaseComment);

                jiraService.updateVersion(version);
            }
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }

    }

    private void recordInConfluence(String projectKey, String confluencePageId, String confluenceQuery, String template, String version, String releaseComment, Map<String, String> customVariables) {

        try {
            ContentResponse response = confluenceService.getContentBodyStorage(confluencePageId);

            String pageBody = response.getBody().getStorage().getValue();

            // Record only when version not exists in the table
            if(containsVersionInPage(confluenceQuery, pageBody, version)) {
                LOGGER.debug("Skipping update of Confluence page ID \"" + confluencePageId + "\". Version " + version + " already released.");
            } else {
                // Generate table record from template
                String tableRecord = fillTemplate(projectKey, template, version, releaseComment, customVariables);
                // Add record of table 
                String newPageBody = insertSnippetIntoPage(confluenceQuery, pageBody, tableRecord, false);
                // Update confluence content
                confluenceService.updatePage(confluencePageId, response.getTitle(), newPageBody);
            }
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }

    }

    public String fillTemplate(String projectKey, String templateContent, String version, String releaseComment, Map<String, String> customVariables) throws TemplateException, IOException {

        Map<String, Object> projectMap = new HashMap<>();
        projectMap.put("jiraProjectKey", projectKey);
        projectMap.put("version", version);
        projectMap.put("releaseComment", releaseComment);

        Map<String, Object> m = new HashMap();
        m.put("j2c", projectMap);
        m.put("cv", customVariables);

        m.put("uuid", (TemplateMethodModelEx)(list)->UUID.randomUUID());

        // Following lines are deprecated variables 
        m.put("project", projectMap);
        m.put("version", version);
        m.put("releaseComment", releaseComment);

        if(!StringUtils.hasText(templateContent)) {
            throw new IOException("Template missing.");
        }

        LOGGER.info("Preparing template for project \"" + projectKey + "\"...");
        try {
            Configuration cfg = initMapper();
            Template template = new Template(null, new StringReader(templateContent), cfg);

            StringWriter out = new StringWriter();

            template.process(m, out);

            return out.toString();
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            LOGGER.error("Template=" + templateContent);
            throw ex;
        }
    }

    private boolean containsVersionInPage(String confluenceJsoupQuery, String pageBody, String version) throws Exception {
        Document doc = Jsoup.parseBodyFragment(pageBody);
        Element element = doc.selectFirst(confluenceJsoupQuery);

        if(element == null) {
            throw new Exception("Jsoup query can't find any parent element.");
        }

        Element parent = element.parent();
        boolean contains = !parent.select("*:contains(" + version + ")").isEmpty();

        return contains;
    }

    public String preview(String confluencePageId, String confluenceJsoupQuery, String record) throws Exception {
        String pageContent = readPageContent(confluencePageId);

        String preview = insertSnippetIntoPage(confluenceJsoupQuery, pageContent, record, true);

        return preview;
    }

    private String insertSnippetIntoPage(String confluenceJsoupQuery, String pageBody, String record, boolean prettyPrint) throws Exception {
        if(!StringUtils.hasText(confluenceJsoupQuery)) {
            throw new Exception("Invalid Jsoup query \"" + confluenceJsoupQuery + "\".");
        }

        Document doc = Jsoup.parseBodyFragment(pageBody);
        Element element = doc.selectFirst(confluenceJsoupQuery);

        if(element == null) {
            throw new Exception("No element found on query \"" + confluenceJsoupQuery + "\".");
        }

        element.after(record);

        doc.outputSettings().prettyPrint(prettyPrint);
        doc.outputSettings().syntax(Document.OutputSettings.Syntax.xml);

        String html = doc.body().html();

        return html;
    }

    private Configuration initMapper() throws IOException {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_27);
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setLogTemplateExceptions(false);
        cfg.setWrapUncheckedExceptions(true);

        return cfg;
    }

}

/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c.services.jira.entities;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import de.elomagic.j2c.services.jira.entities.adapter.StringDeserializer;

/**
 * {
 * "key":"searchRequests",
 * "value": {
 * "ids": []
 * }
 * }
 *
 * @author Carsten Rambow
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ProjectProperty {

    private String key;
    @JsonDeserialize(using = StringDeserializer.class)
    private String value;

    public ProjectProperty() {
    }

    public ProjectProperty(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}

/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c.services.jira;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.util.StringUtils;

import de.elomagic.j2c.services.AbstractService;
import de.elomagic.j2c.services.jira.entities.Project;
import de.elomagic.j2c.services.jira.entities.ProjectPropertiesResponse;
import de.elomagic.j2c.services.jira.entities.ProjectProperty;
import de.elomagic.j2c.services.jira.entities.Version;
import de.elomagic.j2c.settings.J2CSettings;
import de.elomagic.j2c.settings.dto.CredentialsSetting;
import freemarker.template.utility.StringUtil;

/**
 *
 * @author Carsten Rambow
 */
public class JiraService extends AbstractService {

    private static final Logger LOGGER = LogManager.getLogger(JiraService.class);

    private final String REST_API_PATH = "rest/api/2";

    @Inject
    private J2CSettings settings;

    public JiraService() throws Exception {
    }

    @Override
    protected CredentialsSetting getCredentials() {
        return settings.getRoot().getJira().getCredentials();
    }

    @Override
    protected String getServiceURL() throws Exception {
        String baseUrl = settings.getRoot().getJira().getUrl();
        if(!StringUtils.hasText(baseUrl)) {
            throw new Exception("Base URL of Jira not defined. Check configuration or command line syntax.");
        }

        String url = UriBuilder.fromUri(baseUrl).path(REST_API_PATH).build().toString();
        return url;
    }

    public Project getProjectByKey(String projectKey) throws Exception {
        Response response = getClient()
                .target(getServiceURL())
                .path("project")
                .path(projectKey)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        Project project = proceedResponse(response, Project.class, FailMode.ThrowException);

        return project;
    }

    public Version[] getVersionsOfProject(String jiraProjectKey) throws Exception {
        Response response = getClient()
                .target(getServiceURL())
                .path("project")
                .path(jiraProjectKey)
                .path("versions")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        Version[] versions = proceedResponse(response, Version[].class, FailMode.ThrowException);

        return versions;
    }

    /**
     *
     * @param projectKey
     * @param propertyKey
     * @param defaultValue
     * @return Returns an empty object when property doesn't exists but never null
     * @throws Exception
     */
    public ProjectProperty getProjectProperty(String projectKey, String propertyKey, String defaultValue) throws Exception {
        Response response = getClient()
                .target(getServiceURL())
                .path("project")
                .path(projectKey)
                .path("properties")
                .path(propertyKey)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        ProjectProperty result = proceedResponse(response, ProjectProperty.class, FailMode.ReturnNull);

        if(result == null) {
            result = new ProjectProperty(projectKey, defaultValue);
        }

        return result;
    }

    public ProjectPropertiesResponse getProjectProperties(String projectKey) throws Exception {
        Response response = getClient()
                .target(getServiceURL())
                .path("project")
                .path(projectKey)
                .path("properties")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        ProjectPropertiesResponse o = proceedResponse(response, ProjectPropertiesResponse.class, FailMode.ThrowException);

        return o;
    }

    public void setProjectProperty(String projectKey, String propertyKey, String propertyValue) throws Exception {

        String value = propertyValue == null ? "" : StringUtil.jsonStringEnc(propertyValue);

        Entity entity = Entity.json("\"" + value + "\"");

        Response response = getClient()
                .target(getServiceURL())
                .path("project")
                .path(projectKey)
                .path("properties")
                .path(propertyKey)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .put(entity);

        proceedResponse(response, Void.class, FailMode.ThrowException);

        LOGGER.debug("Property \"" + propertyKey + "\" successful set at project \"" + projectKey + "\".");
    }

    private Version getVersionById(String versionId) throws Exception {
        Response response = getClient()
                .target(getServiceURL())
                .path("version")
                .path(versionId)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();

        Version version = proceedResponse(response, Version.class, FailMode.ThrowException);

        return version;
    }

    public Version createVersion(Version v) throws Exception {
        LOGGER.debug("Creating version \"" + v.getName() + "\" of project \"" + v.getProject() + "\".");

        Version version = new Version();
        version.setName(v.getName());
        version.setReleased(v.isReleased());
        version.setReleaseDate(v.getReleaseDate());
        version.setDescription(v.getDescription());
        version.setProjectId(v.getProjectId());
        version.setProject(v.getProject());
        version.setArchived(v.isArchived());

        Entity entity = Entity.json(version);

        Response response = getClient()
                .target(getServiceURL())
                .path("version")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .post(entity);

        version = proceedResponse(response, Version.class, FailMode.ThrowException);

        LOGGER.info("Version \"" + v.getName() + "\" of project \"" + v.getProject() + "\" successful created.");

        return version;
    }

    public void updateVersion(Version v) throws Exception {
        Version version = getVersionById(v.getId());

        if(version != null) {
            LOGGER.debug("Updating version \"" + version.getName() + "\" of project ID \"" + version.getProjectId() + "\" in Jira.");

            version.setReleased(v.isReleased());
            version.setReleaseDate(v.getReleaseDate());
            version.setDescription(v.getDescription());

            Entity entity = Entity.json(version);

            Response response = getClient()
                    .target(getServiceURL())
                    .path("version")
                    .path(v.getId())
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .put(entity);

            proceedResponse(response, Void.class, FailMode.ThrowException);

            LOGGER.info("Version \"" + version.getName() + "\" of project \"" + v.getProject() + "\" successful updated.");
        }
    }

}

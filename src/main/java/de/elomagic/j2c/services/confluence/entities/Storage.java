/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c.services.confluence.entities;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Storage {

    private String value;
    private String representation;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRepresentation() {
        return representation;
    }

    public void setRepresentation(String representation) {
        this.representation = representation;
    }

    public static final class StorageBuilder {

        private String value;
        private String representation;

        private StorageBuilder() {
        }

        public static StorageBuilder aStorage() {
            return new StorageBuilder();
        }

        public StorageBuilder withValue(String value) {
            this.value = value;
            return this;
        }

        public StorageBuilder withRepresentation(String representation) {
            this.representation = representation;
            return this;
        }

        public Storage build() {
            Storage storage = new Storage();
            storage.setValue(value);
            storage.setRepresentation(representation);
            return storage;
        }
    }
}

/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c.services.confluence;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.util.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;

import de.elomagic.j2c.services.AbstractService;
import de.elomagic.j2c.services.confluence.entities.Body;
import de.elomagic.j2c.services.confluence.entities.ContentRequest;
import de.elomagic.j2c.services.confluence.entities.ContentResponse;
import de.elomagic.j2c.services.confluence.entities.Storage;
import de.elomagic.j2c.services.confluence.entities.Version;
import de.elomagic.j2c.settings.J2CSettings;
import de.elomagic.j2c.settings.dto.CredentialsSetting;

/**
 *
 * @author Carsten Rambow
 */
public class ConfluenceService extends AbstractService {

    private static final Logger LOGGER = LogManager.getLogger(ConfluenceService.class);

    private final String REST_API_CONTENT_PATH = "rest/api/content";

    private final ObjectMapper objectMapper;

    @Inject
    private J2CSettings settings;

    public ConfluenceService() {
        objectMapper = new ObjectMapper();
    }

    @Override
    protected CredentialsSetting getCredentials() {
        return settings.getRoot().getConfluence().getCredentials();
    }

    @Override
    protected String getServiceURL() throws Exception {
        String baseUrl = settings.getRoot().getConfluence().getUrl();
        if(!StringUtils.hasText(baseUrl)) {
            throw new Exception("Base URL of Confluence not defined. Check configuration or command line syntax.");
        }
        String url = UriBuilder.fromUri(baseUrl).path(REST_API_CONTENT_PATH).build().toString();
        return url;
    }

    private Invocation.Builder createResponseBuilder(String confluencePageId, Object... expandQueryValues) throws Exception {
        WebTarget target = getClient()
                .target(getServiceURL())
                .path(confluencePageId);

        if(expandQueryValues != null) {
            target = target.queryParam("expand", expandQueryValues);
        }

        Invocation.Builder builder = target.request(MediaType.APPLICATION_JSON);

        return builder;

    }

    private ContentResponse getContent(String confluencePageId, Object... expandValues) throws Exception {
        Response response = createResponseBuilder(confluencePageId, expandValues).get();

        ContentResponse contentResponse = proceedResponse(response, ContentResponse.class, FailMode.ThrowException);

        return contentResponse;
    }

    private ContentResponse getContentVersion(String confluencePageId) throws Exception {
        return getContent(confluencePageId, "version");
    }

    public ContentResponse getContentBodyStorage(String confluencePageId) throws Exception {
        return getContent(confluencePageId, "body.storage");
    }

    public ContentResponse updatePage(String confluencePageId, String title, String content) throws Exception {
        Integer previousVersion = getContentVersion(confluencePageId).getVersion().getNumber();

        ContentRequest.ContentRequestBuilder updateContentRequestBuilder = ContentRequest.ContentRequestBuilder.aContentRequest();
        int newVersion = previousVersion + 1;

        ContentRequest contentRequest = updateContentRequestBuilder
                .withType("page")
                .withTitle(title)
                .withVersion(new Version(newVersion))
                .withBody(Body.BodyBuilder.aBody()
                        .withStorage(Storage.StorageBuilder.aStorage()
                                .withValue(content)
                                .withRepresentation("storage").build())
                        .build())
                .build();

        Entity entity = Entity.json(contentRequest);

        Response response = createResponseBuilder(confluencePageId).put(entity);

        ContentResponse createContentResponse = proceedResponse(response, ContentResponse.class, FailMode.ThrowException);

        LOGGER.info("Confluence page with id " + createContentResponse.getId() + " successful updated.");
        return createContentResponse;
    }

}

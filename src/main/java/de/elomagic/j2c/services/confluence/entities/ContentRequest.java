/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c.services.confluence.entities;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ContentRequest {

    private String id;
    private Version version;
    private String title;
    private String type;
    private Page[] ancestors;
    private Space space;
    private Body body;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Space getSpace() {
        return space;
    }

    public void setSpace(Space space) {
        this.space = space;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Page[] getAncestors() {
        return ancestors;
    }

    public void setAncestors(Page[] ancestors) {
        this.ancestors = ancestors;
    }

    public static final class ContentRequestBuilder {

        private String id;
        private String type;
        private String title;
        private Page[] ancestors;
        private Space space;
        private Body body;
        private Version version;

        private ContentRequestBuilder() {
        }

        public static ContentRequestBuilder aContentRequest() {
            return new ContentRequestBuilder();
        }

        public ContentRequestBuilder withId(String id) {
            this.id = id;
            return this;
        }

        public ContentRequestBuilder withType(String type) {
            this.type = type;
            return this;
        }

        public ContentRequestBuilder withTitle(String title) {
            this.title = title;
            return this;
        }

        public ContentRequestBuilder withAncestors(Page[] ancestors) {
            this.ancestors = ancestors;
            return this;
        }

        public ContentRequestBuilder withSpace(Space space) {
            this.space = space;
            return this;
        }

        public ContentRequestBuilder withBody(Body body) {
            this.body = body;
            return this;
        }

        public ContentRequestBuilder withVersion(Version version) {
            this.version = version;
            return this;
        }

        public ContentRequest build() {
            ContentRequest contentRequest = new ContentRequest();
            contentRequest.setId(id);
            contentRequest.setType(type);
            contentRequest.setTitle(title);
            contentRequest.setAncestors(ancestors);
            contentRequest.setSpace(space);
            contentRequest.setBody(body);
            contentRequest.setVersion(version);
            return contentRequest;
        }
    }
}

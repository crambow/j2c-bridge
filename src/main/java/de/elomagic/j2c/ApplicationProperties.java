/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c;

import java.net.URI;

import de.elomagic.core.app.AbstractConsoleAppProperties;
import de.elomagic.core.app.ConsoleAppParameter;

/**
 * Reads properties from the resource file <code>pom.properties</code> or <code>MANIFEST.MF</code>.
 * <p>
 *
 * @author Carsten Rambow
 */
public final class ApplicationProperties extends AbstractConsoleAppProperties {

    @Override
    public String getPomXmlName() {
        return "/META-INF/maven/de.elomagic/j2c-bridge/pom.xml";
    }

    @Override
    public String getPomPropertiesName() {
        return "/META-INF/maven/de.elomagic/j2c-bridge/pom.properties";
    }

    @Override
    protected ConsoleAppParameter[] getSupportedParameters() {
        return SupportedParameters.values();
    }

    @Override
    public URI getHomepageURL() {
        return URI.create("https://bitbucket.org/crambow/j2c-bridge");
    }

    @Override
    public String getCommandLineSyntax() {
        return "j2c <command> [options]";
    }

}

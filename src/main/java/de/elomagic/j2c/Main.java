/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.shiro.util.StringUtils;

import de.elomagic.core.app.AbstractConsoleApp;
import de.elomagic.core.app.ConsoleAppExitCode;
import de.elomagic.core.app.ConsoleAppParameters;
import de.elomagic.core.utils.SimpleCrypt;
import de.elomagic.j2c.services.J2CServiceBean;
import de.elomagic.j2c.services.jira.entities.ProjectProperty;
import de.elomagic.j2c.settings.J2CSettings;

/**
 * Main class of the project.
 *
 * @author Carsten Rambow
 */
public class Main extends AbstractConsoleApp {

    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    @Inject
    private J2CSettings settings;
    @Inject
    private J2CServiceBean service;
    @Inject
    private ApplicationProperties applicationProperties;

    /**
     * Main start method.
     * <p>
     * Note: Some <code>System.out</code> coding inside. Do not replace them otherwise private content will be logged in a separate file.
     *
     * @param cp
     * @return
     */
    @Override
    protected ConsoleAppExitCode start(ConsoleAppParameters cp) {
        String file = cp.getValue(SupportedParameters.OptConfigurationFile, null);
        if(file != null) {
            try {
                settings.read(Paths.get(file));
            } catch(Exception ex) {
                LOGGER.error("Unable to read configuration file \"" + file + "\".", ex);
                System.err.println("Unable to read configuration file \"" + file + "\": " + ex.getMessage());
                printHelp();
                return ConsoleAppExitCode.ConfigurationFileNotFound;
            }
        }

        try {
            cp.onOption(SupportedParameters.OptVerbose, v->Configurator.setLevel(Main.class.getPackage().getName(), Level.DEBUG));
            cp.onOption(SupportedParameters.OptConfluenceUrl, url->settings.getRoot().getConfluence().setUrl(url));
            cp.onOption(SupportedParameters.OptConfluencePassword, pwd->settings.getRoot().getConfluence().getCredentials().setPassword(pwd));
            cp.onOption(SupportedParameters.OptConfluenceUsername, uid->settings.getRoot().getConfluence().getCredentials().setUsername(uid));
            cp.onOption(SupportedParameters.OptJiraUrl, url->settings.getRoot().getJira().setUrl(url));
            cp.onOption(SupportedParameters.OptJiraPassword, pwd->settings.getRoot().getJira().getCredentials().setPassword(pwd));
            cp.onOption(SupportedParameters.OptJiraUsername, uid->settings.getRoot().getJira().getCredentials().setUsername(uid));

            if(cp.containsOption(SupportedParameters.CmdCreateTemplateSample)) {
                LOGGER.debug("Create template sample...");
                Path templateFile = cp.getValueAsPath(SupportedParameters.OptTemplateFile, null);
                service.createSample(templateFile);
            } else if(cp.containsOption(SupportedParameters.CmdEncryptPassword)) {
                LOGGER.debug("Encrypt password...");
                String password = cp.getValue(SupportedParameters.CmdEncryptPassword);
                String ep = SimpleCrypt.encrypt(password);
                System.out.println(ep);
            } else if(cp.containsOption(SupportedParameters.CmdExport)) {
                LOGGER.debug("Export template...");
                String key = cp.getValue(SupportedParameters.OptJiraProjectKey);
                Path templateFile = cp.getValueAsPath(SupportedParameters.OptTemplateFile, null);
                service.exportTemplateFromJira(key, templateFile);
            } else if(cp.containsOption(SupportedParameters.CmdHelp)) {
                printHelp();
            } else if(cp.containsOption(SupportedParameters.CmdImport)) {
                LOGGER.debug("Import project properties...");
                String key = cp.getValue(SupportedParameters.OptJiraProjectKey);
                if(cp.containsOption(SupportedParameters.OptConfluencePageId)) {
                    service.importPageId(key, cp.getValue(SupportedParameters.OptConfluencePageId));
                }
                if(cp.containsOption(SupportedParameters.OptTemplateFile)) {
                    service.importRecordTemplate(
                            key,
                            cp.getValueAsPath(SupportedParameters.OptTemplateFile, null),
                            getCustomizedVariables(cp));
                }
                if(cp.containsOption(SupportedParameters.OptJSoupQuery)) {
                    service.importJSoupQuery(key, cp.getValue(SupportedParameters.OptJSoupQuery));
                }
            } else if(cp.containsOption(SupportedParameters.CmdPrintPage)) {
                LOGGER.debug("Print Confluence page...");
                String pageId = getConfluencePageId(cp);
                String pageContent = service.readPageContent(pageId);
                System.out.println(pageContent);
            } else if(cp.containsOption(SupportedParameters.CmdPrintProjectProperties)) {
                LOGGER.debug("Print project properties...");
                String key = cp.getValue(SupportedParameters.OptJiraProjectKey);
                Set<ProjectProperty> pp = service.readProjectPropertiesFromJira(key);
                pp.forEach(item->System.out.println(item.getKey() + "=" + item.getValue()));
            } else if(cp.containsOption(SupportedParameters.CmdPreviewPage)) {
                LOGGER.debug("Preview Confluence page...");
                String pageId = getConfluencePageId(cp);
                String query = getConfluenceJsoupQuery(cp);
                String preview = service.preview(pageId, query, "<!-- ***** Record will be inserted here !!! **** -->");
                System.out.println(preview);
            } else if(cp.containsOption(SupportedParameters.CmdValidateTemplate)) {
                LOGGER.debug("Validate template...");
                String version = cp.getValue(SupportedParameters.OptVersion, "1.2.3.4");
                String comment = cp.getValue(SupportedParameters.OptReleaseComment, "ReleaseComment");
                String template = getTemplateContent(cp);

                String snippet = service.fillTemplate(
                        "KEY",
                        template,
                        version,
                        comment,
                        getCustomizedVariables(cp));

                System.out.println(snippet);
            } else if(cp.containsOption(SupportedParameters.CmdRelease)) {
                LOGGER.debug("Release version...");
                String projectVersion = cp.getValue(SupportedParameters.OptVersion);
                String releaseComment = cp.getValue(SupportedParameters.OptReleaseComment, "");
                boolean skipConfluence = cp.containsOption(SupportedParameters.OptSkipConfluence);
                boolean skipJira = cp.containsOption(SupportedParameters.OptSkipJira);
                LocalDate releaseDate = cp.getValueAsLocalDate(SupportedParameters.OptReleaseDate, LocalDate.now());

                service.recordReleasedVersion(
                        cp.getValue(SupportedParameters.OptJiraProjectKey),
                        getConfluencePageId(cp),
                        getConfluenceJsoupQuery(cp),
                        getTemplateContent(cp),
                        projectVersion,
                        skipJira,
                        skipConfluence,
                        releaseDate,
                        releaseComment,
                        getCustomizedVariables(cp));
            } else if(cp.containsOption(SupportedParameters.CmdVersion)) {
                String version = applicationProperties.getDisplayNameVersion();
                System.out.println(version);
            } else {
                throw new ParseException("No command entered.");
            }
        } catch(ParseException ex) {
            LOGGER.error("Parameter syntax error: " + ex.getMessage(), ex);
            printHelp();
            return ConsoleAppExitCode.SyntaxError;
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return ConsoleAppExitCode.SyntaxError;
        }

        return null;
    }

    private String getConfluencePageId(ConsoleAppParameters cp) throws Exception {
        if(cp.containsOption(SupportedParameters.OptConfluencePageId)) {
            return cp.getValue(SupportedParameters.OptConfluencePageId);
        }

        if(!cp.containsOption(SupportedParameters.OptJiraProjectKey)) {
            throw new Exception("Command line must contains the Confluence page ID or the Jira project key!");
        }

        String confluencePageId = service.readConfluencePageIdFromJira(cp.getValue(SupportedParameters.OptJiraProjectKey));

        if(!StringUtils.hasText(confluencePageId)) {
            throw new Exception("No Confluence page ID found. Confluence page ID must be set as console parameter or persisted as property of the Jira project.");
        }

        return confluencePageId;
    }

    private String getTemplateContent(ConsoleAppParameters cp) throws Exception {
        String filename = cp.getValue(SupportedParameters.OptTemplateFile, null);
        if(filename == null) {
            String projectKey = cp.getValue(SupportedParameters.OptJiraProjectKey);
            String template = service.readTemplateFromJira(projectKey);
            return template;
        } else {
            Path file = Paths.get(filename);
            String template = service.readTemplate(file);
            return template;
        }
    }

    private String getConfluenceJsoupQuery(ConsoleAppParameters cp) throws Exception {
        if(cp.containsOption(SupportedParameters.OptJSoupQuery)) {
            return cp.getValue(SupportedParameters.OptJSoupQuery);
        }

        if(!cp.containsOption(SupportedParameters.OptJiraProjectKey)) {
            throw new Exception("Command line must contains the Jsoup query or the Jira project key!");
        }

        String query = service.readJsoupQueryFromJira(cp.getValue(SupportedParameters.OptJiraProjectKey));

        if(!StringUtils.hasText(query)) {
            throw new Exception("No Jsoup query found. The Jsoup query must be set as console parameter or persisted as property of the Jira project.");
        }

        return query;
    }

    private Map<String, String> getCustomizedVariables(ConsoleAppParameters cp) {
        Map<String, String> result = new HashMap<>();

        cp.getValueProperties(SupportedParameters.OptCustomVariable)
                .entrySet()
                .forEach(item->result.put(item.getKey().toString(), item.getValue().toString()));

        return result;
    }

}

/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c.settings.dto;

import de.elomagic.core.settings.annotations.SettingsProperty;
import de.elomagic.core.utils.SimpleCrypt;

/**
 *
 * @author Carsten Rambow
 */
public class CredentialsSetting {

    private String username;
    @SettingsProperty(secure = true)
    private String password;
    private String domain;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    /**
     * Returns the password of the credentials.
     * <p>
     * In case when password is encrypted then it will be returned decrypted.
     *
     * @return The password
     * @throws Exception
     */
    public String getPasswordDecrypted() throws Exception {

        String result = password;

        if(SimpleCrypt.isEncryptedValue(result)) {
            result = SimpleCrypt.decrypt(result);
        }

        return result;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

}

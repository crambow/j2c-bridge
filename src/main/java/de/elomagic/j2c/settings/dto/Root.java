/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c.settings.dto;

import de.elomagic.core.settings.annotations.SettingsRoot;

/**
 *
 * @author Carsten Rambow
 */
@SettingsRoot(templateResource = "/de/elomagic/j2c/samples/configuration.json", encryptPasswords = true)
public class Root {

    private String version = "1.0.0";
    private ConnectionSetting jira = new ConnectionSetting();
    private ConnectionSetting confluence = new ConnectionSetting();

    public ConnectionSetting getJira() {
        return jira;
    }

    public ConnectionSetting getConfluence() {
        return confluence;
    }

    public String getVersion() {
        return version;
    }

}

/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c.settings;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;

import de.elomagic.core.settings.Settings;
import de.elomagic.j2c.settings.dto.Root;

/**
 *
 * @author Carsten Rambow
 */
@ApplicationScoped
public class J2CSettings extends Settings<Root> {

    @PostConstruct
    private void postConstruct() throws Exception {
        clear();
    }

    @Override
    protected Class<Root> getRootClass() {
        return Root.class;
    }

}

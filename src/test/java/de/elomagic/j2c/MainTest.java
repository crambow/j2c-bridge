/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c;

import java.io.InputStreamReader;
import java.io.PrintStream;

import javax.inject.Inject;

import com.github.tomakehurst.wiremock.WireMockServer;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.weld.junit5.EnableWeld;
import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldSetup;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import de.elomagic.core.app.ConsoleAppExitCode;
import de.elomagic.core.app.ConsoleAppParameters;
import de.elomagic.j2c.services.J2CServiceBean;
import de.elomagic.j2c.services.confluence.ConfluenceService;
import de.elomagic.j2c.services.jira.JiraService;
import de.elomagic.j2c.settings.J2CSettings;
import de.elomagic.j2c.testtools.HttpsWiremockConfigFactory;
import de.elomagic.j2c.testtools.TestUtils;
import de.elomagic.j2c.testtools.endpoints.DefaultServiceEndpoints;
import de.elomagic.j2c.testtools.endpoints.RecordReleaseServiceEndpoints;
import ru.lanwen.wiremock.ext.WiremockResolver;

@EnableWeld
@ExtendWith({
    WiremockResolver.class
})
public class MainTest {

    private static final Logger LOGGER = LogManager.getLogger(MainTest.class);

    @WeldSetup
    public WeldInitiator weld = WeldInitiator.of(
            Main.class,
            J2CSettings.class,
            J2CServiceBean.class,
            JiraService.class,
            ConfluenceService.class,
            ConsoleAppParametersProducer.class,
            ApplicationProperties.class
    );

    @Inject
    private ConsoleAppParameters consoleAppParameters;
    @Inject
    private J2CSettings settings;
    @Inject
    private Main main;

    @BeforeEach
    public void beforeMainTest() throws Exception {
        settings.read(new InputStreamReader(getClass().getResourceAsStream("/configuration4test.json")));
    }

    @Test
    public void testPrintHelp() throws Exception {
        LOGGER.info("Starting testMain1...");
        PrintStream backup = System.out;
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try (PrintStream ps = new PrintStream(out, true, "utf-8")) {
                System.setOut(ps);

                consoleAppParameters.parse(new String[] {"-help"});

                main.start(consoleAppParameters);
                ps.flush();
            }
            String expect = "Encrypt the given";
            String value = out.toString("utf-8");
            Assertions.assertTrue(value.contains(expect), "Value \"" + value + "\" doesn't contains string \"" + expect + "\". Currently is:" + value);
        } finally {
            System.setOut(backup);
        }
    }

    @Test
    public void testPrintProjectFailed() throws Exception {

        consoleAppParameters.parse(new String[] {"-pp", "-key", "ABC"});

        ConsoleAppExitCode exitCode = main.start(consoleAppParameters);

        Assertions.assertEquals(ConsoleAppExitCode.SyntaxError, exitCode);

    }

    @Test
    public void testEncryptPassword() throws Exception {
        PrintStream backup = System.out;
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try (PrintStream ps = new PrintStream(out, true, "utf-8")) {
                System.setOut(ps);

                consoleAppParameters.parse(new String[] {"-ep", "ABC"});

                ConsoleAppExitCode exitCode = main.start(consoleAppParameters);

                Assertions.assertNull(exitCode);

                ps.flush();
            }
            String expect = "{";
            String value = out.toString("utf-8");
            Assertions.assertTrue(value.contains(expect), "Value \"" + value + "\" doesn't contains string \"" + expect + "\".");
        } finally {
            System.setOut(backup);
        }

    }

    @Test
    public void testPrintProjectProperties(
            @WiremockResolver.Wiremock(customizer = DefaultServiceEndpoints.class,
                                       factory = HttpsWiremockConfigFactory.class) WireMockServer server) throws Exception {

        settings.getRoot().getJira().setUrl(TestUtils.getSecureUri(server));
        settings.getRoot().getConfluence().setUrl(TestUtils.getSecureUri(server));

        PrintStream backup = System.out;
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            try (PrintStream ps = new PrintStream(out, true, "utf-8")) {
                System.setOut(ps);

                consoleAppParameters.parse(new String[] {"-ppp", "-key", "RPS"});

                ConsoleAppExitCode exitCode = main.start(consoleAppParameters);

                Assertions.assertNull(exitCode);

                ps.flush();
            }
            String expect = "j2c.confluenceRecordTemplate=template";
            String value = out.toString("utf-8");
            Assertions.assertTrue(value.contains(expect), "Value \"" + value + "\" doesn't contains string \"" + expect + "\".");
        } finally {
            System.setOut(backup);
        }

    }

    @Test
    public void testRelease(
            @WiremockResolver.Wiremock(customizer = RecordReleaseServiceEndpoints.class,
                                       factory = HttpsWiremockConfigFactory.class) WireMockServer server) throws Exception {

        settings.getRoot().getJira().setUrl(TestUtils.getSecureUri(server));
        settings.getRoot().getConfluence().setUrl(TestUtils.getSecureUri(server));

        consoleAppParameters.parse(new String[] {"-r", "-key", "RPS", "-ver", "4.3.2.1", "-rc", "simpleComment", "-rd", "2001-02-01", "-Ckey1=value1", "-Ckey2=value2"});

        ConsoleAppExitCode exitCode = main.start(consoleAppParameters);

        Assertions.assertNull(exitCode);
    }

}

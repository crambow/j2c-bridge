/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c.testtools.endpoints;

import java.io.IOException;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;

import org.apache.commons.io.IOUtils;

import ru.lanwen.wiremock.config.WiremockCustomizer;

/**
 *
 * @author Carsten Rambow
 */
public class ReadPageServiceEndpoints implements WiremockCustomizer {

    @Override
    public void customize(WireMockServer server) {
        try {
            // Get confluence page content
            server.stubFor(WireMock.get(WireMock.urlEqualTo("/rest/api/content/abc?expand=body.storage"))
                    .withBasicAuth("user2", "secret2")
                    .withHeader("Accept", WireMock.equalTo("application/json"))
                    .willReturn(WireMock.aResponse()
                            .withStatus(200)
                            .withHeader("Content-Type", "application/json")
                            .withBody(IOUtils.toByteArray(getClass().getResourceAsStream("/confluence-get-content-response-default.json")))));
        } catch(IOException ex) {
            throw new RuntimeException(ex);
        }
    }

}

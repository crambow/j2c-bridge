/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c.testtools.endpoints;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;

import ru.lanwen.wiremock.config.WiremockCustomizer;

/**
 *
 * @author Carsten Rambow
 */
public class SetProjectPropertiesServiceEndpoints implements WiremockCustomizer {

    @Override
    public void customize(WireMockServer server) {
        server.stubFor(WireMock.put(WireMock.urlEqualTo("/rest/api/2/project/ABC/properties/j2c.comment"))
                .withBasicAuth("user1", "secret1")
                .withHeader("Accept", WireMock.equalTo("application/json"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)));
        server.stubFor(WireMock.put(WireMock.urlEqualTo("/rest/api/2/project/ABC/properties/j2c.version"))
                .withBasicAuth("user1", "secret1")
                .withHeader("Accept", WireMock.equalTo("application/json"))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)));
        server.stubFor(WireMock.put(WireMock.urlEqualTo("/rest/api/2/project/ABC/properties/j2c.confluenceRecordTemplate"))
                .withBasicAuth("user1", "secret1")
                .withHeader("Accept", WireMock.equalTo("application/json"))
                .withRequestBody(WireMock.equalToJson("\"JustAnotherTest\""))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)));
        server.stubFor(WireMock.put(WireMock.urlEqualTo("/rest/api/2/project/ABC/properties/j2c.confluenceJsoupQuery"))
                .withBasicAuth("user1", "secret1")
                .withHeader("Accept", WireMock.equalTo("application/json"))
                .withRequestBody(WireMock.equalToJson("\"<td>\""))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)));
        server.stubFor(WireMock.put(WireMock.urlEqualTo("/rest/api/2/project/ABC/properties/j2c.confluencePageId"))
                .withBasicAuth("user1", "secret1")
                .withHeader("Accept", WireMock.equalTo("application/json"))
                .withRequestBody(WireMock.equalToJson("\"4711\""))
                .willReturn(WireMock.aResponse()
                        .withStatus(200)));
    }

}

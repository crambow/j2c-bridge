/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c.services;

import java.io.File;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Set;

import javax.inject.Inject;

import com.github.tomakehurst.wiremock.WireMockServer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jboss.weld.junit5.EnableWeld;
import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldSetup;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import de.elomagic.j2c.ApplicationProperties;
import de.elomagic.j2c.services.confluence.ConfluenceService;
import de.elomagic.j2c.services.jira.JiraService;
import de.elomagic.j2c.services.jira.entities.ProjectProperty;
import de.elomagic.j2c.settings.J2CSettings;
import de.elomagic.j2c.testtools.HttpsWiremockConfigFactory;
import de.elomagic.j2c.testtools.TestUtils;
import de.elomagic.j2c.testtools.endpoints.GetProjectPropertiesServiceEndpoints;
import de.elomagic.j2c.testtools.endpoints.ReadPageServiceEndpoints;
import de.elomagic.j2c.testtools.endpoints.RecordReleaseServiceEndpoints;
import de.elomagic.j2c.testtools.endpoints.SetProjectPropertiesServiceEndpoints;
import ru.lanwen.wiremock.ext.WiremockResolver;
import ru.lanwen.wiremock.ext.WiremockResolver.Wiremock;

/**
 *
 * @author Carsten Rambow
 */
@EnableWeld
@ExtendWith({
    WiremockResolver.class
})
class J2CServiceBeanTest {

    @Inject
    J2CServiceBean bean;
    @Inject
    J2CSettings settings;

    @WeldSetup
    public WeldInitiator weld = WeldInitiator.of(
            J2CServiceBean.class,
            JiraService.class,
            ConfluenceService.class,
            J2CSettings.class,
            ApplicationProperties.class
    );

    @BeforeEach
    void beforeEach() throws Exception {
        settings.read(new InputStreamReader(getClass().getResourceAsStream("/configuration4test.json")));
    }

    @Test
    void testReadPage(
            @Wiremock(customizer = ReadPageServiceEndpoints.class,
                      factory = HttpsWiremockConfigFactory.class) WireMockServer server) throws Exception {

        settings.getRoot().getJira().setUrl(TestUtils.getSecureUri(server));
        settings.getRoot().getConfluence().setUrl(TestUtils.getSecureUri(server));

        String content = bean.readPageContent("abc");

        Assertions.assertEquals("PageContent", content);
    }

    @Test
    void testReadJsoupQueryFromJira(
            @Wiremock(customizer = GetProjectPropertiesServiceEndpoints.class,
                      factory = HttpsWiremockConfigFactory.class) WireMockServer server) throws Exception {

        settings.getRoot().getJira().setUrl(TestUtils.getSecureUri(server));
        settings.getRoot().getConfluence().setUrl(TestUtils.getSecureUri(server));

        Assertions.assertEquals("user1", settings.getRoot().getJira().getCredentials().getUsername());
        Assertions.assertEquals("secret1", settings.getRoot().getJira().getCredentials().getPasswordDecrypted());

        String value = bean.readJsoupQueryFromJira("RPS");

        Assertions.assertEquals("query", value);
    }

    @Test
    void testReadConfluencePageIdFromJira(
            @Wiremock(customizer = GetProjectPropertiesServiceEndpoints.class,
                      factory = HttpsWiremockConfigFactory.class) WireMockServer server) throws Exception {

        settings.getRoot().getJira().setUrl(TestUtils.getSecureUri(server));
        settings.getRoot().getConfluence().setUrl(TestUtils.getSecureUri(server));

        Assertions.assertEquals("user1", settings.getRoot().getJira().getCredentials().getUsername());
        Assertions.assertEquals("secret1", settings.getRoot().getJira().getCredentials().getPasswordDecrypted());

        String value = bean.readConfluencePageIdFromJira("RPS");

        Assertions.assertEquals("pageId", value);
    }

    @Test
    void testReadTemplateFromJira(
            @Wiremock(customizer = GetProjectPropertiesServiceEndpoints.class,
                      factory = HttpsWiremockConfigFactory.class) WireMockServer server) throws Exception {

        settings.getRoot().getJira().setUrl(TestUtils.getSecureUri(server));
        settings.getRoot().getConfluence().setUrl(TestUtils.getSecureUri(server));

        Assertions.assertEquals("user1", settings.getRoot().getJira().getCredentials().getUsername());
        Assertions.assertEquals("secret1", settings.getRoot().getJira().getCredentials().getPasswordDecrypted());

        String value = bean.readTemplateFromJira("RPS");

        Assertions.assertEquals("template", value);
    }

    @Test
    void testReadProjectPropertiesFromJira(
            @Wiremock(customizer = GetProjectPropertiesServiceEndpoints.class,
                      factory = HttpsWiremockConfigFactory.class) WireMockServer server) throws Exception {

        settings.getRoot().getJira().setUrl(TestUtils.getSecureUri(server));
        settings.getRoot().getConfluence().setUrl(TestUtils.getSecureUri(server));

        Set<ProjectProperty> set = bean.readProjectPropertiesFromJira("RPS");
        Assertions.assertEquals(3, set.size());

        Assertions.assertThrows(Exception.class, ()->bean.readProjectPropertiesFromJira("UNKNOWN"));
    }

    @Test
    void testImportPageId(
            @Wiremock(customizer = SetProjectPropertiesServiceEndpoints.class,
                      factory = HttpsWiremockConfigFactory.class) WireMockServer server) throws Exception {

        settings.getRoot().getJira().setUrl(TestUtils.getSecureUri(server));
        settings.getRoot().getConfluence().setUrl(TestUtils.getSecureUri(server));

        bean.importPageId("ABC", "4711");
    }

    @Test
    void testImportJSoupQuery(
            @Wiremock(customizer = SetProjectPropertiesServiceEndpoints.class,
                      factory = HttpsWiremockConfigFactory.class) WireMockServer server) throws Exception {

        settings.getRoot().getJira().setUrl(TestUtils.getSecureUri(server));
        settings.getRoot().getConfluence().setUrl(TestUtils.getSecureUri(server));

        bean.importJSoupQuery("ABC", "<td>");
    }

    @Test
    void testImportRecordTemplate(
            @Wiremock(customizer = SetProjectPropertiesServiceEndpoints.class,
                      factory = HttpsWiremockConfigFactory.class) WireMockServer server) throws Exception {

        File file = File.createTempFile("unittest", ".tmp");
        file.deleteOnExit();

        FileUtils.write(file, "JustAnotherTest", "utf-8");

        settings.getRoot().getJira().setUrl(TestUtils.getSecureUri(server));
        settings.getRoot().getConfluence().setUrl(TestUtils.getSecureUri(server));

        bean.importRecordTemplate("ABC", file.toPath(), Collections.emptyMap());
    }

    @Test
    void testRecordReleasedVersion(
            @Wiremock(customizer = RecordReleaseServiceEndpoints.class,
                      factory = HttpsWiremockConfigFactory.class) WireMockServer server) throws Exception {

        settings.getRoot().getJira().setUrl(TestUtils.getSecureUri(server));
        settings.getRoot().getConfluence().setUrl(TestUtils.getSecureUri(server));

        bean.recordReleasedVersion(
                "xyz",
                "4711",
                "html > body > div",
                IOUtils.toString(getClass().getResourceAsStream("/template.ftlh"), StandardCharsets.UTF_8),
                "1.2.3.4",
                true,
                false,
                LocalDate.now(),
                "release comment",
                Collections.emptyMap());
    }

}

/*
 * Jira2Confluence Bridge
 * Copyright (c) 2018-2018 Carsten Rambow
 * mailto:developer AT elomagic DOT de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.elomagic.j2c;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SupportedParametersTest {

    @Test
    public void testDuplicateOnShortParameters() throws Exception {
        Set<String> commandNames = Arrays.stream(SupportedParameters.values())
                .map(item->item.getShortName())
                .collect(Collectors.toSet());
        Assertions.assertEquals(SupportedParameters.values().length, commandNames.size());
    }

    @Test
    public void testDuplicateOnLongParameters() throws Exception {
        Set<String> commandNames = Arrays.stream(SupportedParameters.values())
                .map(item->item.getLongName())
                .collect(Collectors.toSet());

        Assertions.assertEquals(SupportedParameters.values().length, commandNames.size());
    }

}

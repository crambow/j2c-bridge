#!/bin/bash

sed -i~ "/<servers>/ a\
<server>\
  <id>elo-repository-public</id>\
  <username>$REP_UID</username>\
  <password>$REP_PWD</password>\
</server>" /usr/share/maven/conf/settings.xml


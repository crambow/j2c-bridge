# Jira2Confluence Bridge

[![Dependency Status](https://www.versioneye.com/user/projects/5a6ed6010fb24f59445a0bc8/badge.svg?style=flat-square)](https://www.versioneye.com/user/projects/5a6ed6010fb24f59445a0bc8)
[![codecov](https://codecov.io/bb/crambow/j2c-bridge/branch/master/graph/badge.svg?token=hdxJS34irO)](https://codecov.io/bb/crambow/j2c-bridge)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

The Jira2Confluence Bridge (`j2c`) is a command line tool to ease up documentation of a released version in Jira and Confluence.
A project version in Jira can be released with date and comment and the related Confluence page can be enriched with content. 

## Where can I download the latest binary version?

The latest binary version can be found on [Bitbucket](https://bitbucket.org/crambow/j2c-bridge/downloads).

## How to build a distribution package by myself?

What you need is an installed JDK version 8 and [Apache Maven](https://maven.apache.org).
Then clone this project to your local file system and execute `mvn clean install` in the project folder. After successful finish you find 
the distribution package in the `target` folder.

## FAQ

### How do I create a J2C setup for a project?

Preconditions:
* Project key of an existing Jira project
* Confluence page ID where to record the releases
* Configuration file at least with the credentials of Jira

Approach:
1. Import Confluence page ID into Jira by executing `j2c -in -key [JIRA_KEY] -id [CONFLUENCE_PAGE_ID]`
2. Import record template `j2c -in -key [JIRA_KEY] -tf [RECORD_TEMPLATE_FILE]`
3. Import JSoup query `j2c -in -key [JIRA_KEY] -jsq [JSOUP_QUERY]`

### How do I record a release in Jira and Confluence?

Preconditions:
* Project key of an existing Jira project
* Confluence page ID where to record the releases
* Configuration file at least with the credentials of Jira

Approach:
1. Execute command `j2c -r  -key [JIRA_KEY] -ver [VERSION] -cf [CONF_FILE]`

### What variables and functions are supported in the template? ###

Following variables and functions are supported:

| Variable / Function  | Description             | Sample                 | 
| -------------------  | ----------------------- | ---------------------- |
| `j2c.jiraProjectKey` | Project key             | `ABC`                  |
| `j2c.version`        | Version                 | `1.0.0-SNAPSHOT`       |
| `j2c.releaseComment` | Release comment         | `Some release comment` |
| `uuid()`             | Generates a UUID string | `2eea16b1-45eb-4ff3-8bde-64fdd8c0c642` |

Following variables are deprecated:

| Variable         | Replaced By          | 
| ---------------- | -------------------- |
| `project.*`      | `j2c.*`              |
| `version`        | `j2c.version`        |
| `releaseComment` | `j2c.releaseComment` |

### Can I use customized variables in the template? ###

Yes. The command options for this feature is `-Ckey=value`.

Example: 
`j2c -r -key [JIRA_KEY] -ver [VERSION] -cf [CONF_FILE] -Ckey1=value1 -Ckey2=value2`

...and the template must contains

    ...
    <element>${cv.key1}</element>
    <element>${cv.key2}</element>
    ...

### How do I get the Confluence page ID?

Preconditions:
* The existing page in Confluence

Approach:
1. Open your page in Confluence.
2. Go to _Page Information_  of the Confluence page.
3. You find the page ID as attribute `pageId` of the page URL.

## Who do I talk to? ###

* Repo owner or admin

## License

The Jira2Confluence Bridge is distributed under [GNU General Public License](http://www.gnu.org/licenses)
